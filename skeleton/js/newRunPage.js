// Code for the Measure Run page.


let caulfield = [145.0420733, -37.8770097];

let map = new mapboxgl.Map({
    container: 'map',
    style: 'mapbox://styles/mapbox/streets-v10',
    zoom: 16,
    center: caulfield
});
let markerStart = new mapboxgl.Marker({
    color: "#00a731"
});
let markerEnd = new mapboxgl.Marker({
    color: "#a72600"
});
let markerCurrent = new mapboxgl.Marker({
    color: "#d6ce00"
});
let popupStart = new mapboxgl.Popup({
    closeButton: false,
    closeOnClick: false,
    offset: 25
});
let popupDestination = new mapboxgl.Popup({
    closeButton: false,
    closeOnClick: false,
    offset: 25
});

let current = null;
let run = null;
let wpid;
let restart = false;
let runIndex = undefined;
let startLocation = null;
let destination = null;

function initPage() {
    loadRunList();
    document.getElementById("nickname").value = `Run ${savedRuns.length + 1}`;
    restart = JSON.parse(localStorage.getItem(APP_PREFIX + "-restart"));
    if (restart) {
        runIndex = parseInt(localStorage.getItem(APP_PREFIX + "-selectedRun"));
        if (runIndex in savedRuns) {
            startLocation = savedRuns[runIndex].getStartLocation();
            destination = savedRuns[runIndex].getDestination();
            run = new Run(startLocation, destination);
            document.getElementById("nickname").value = savedRuns[runIndex].getNickname();
            markerStart.setLngLat(startLocation.getLngLat()).addTo(map);
            markerEnd.setLngLat(destination.getLngLat()).addTo(map);
            displayMessage("Please go to start location.", 10000);
        }
    }
    wpid = navigator.geolocation.watchPosition(geo_success, geo_error, {enableHighAccuracy: true, maximumAge: 0, timeout: 60000});
}

window.addEventListener("pageshow", function (event) {
    initPage();
});

let geojson = {
    "type": "FeatureCollection",
    "features": []
};

let linestring = {
    "type": "Feature",
    "geometry": {
        "type": "LineString",
        "coordinates": []
    }
};

map.on('load', function () {
    map.addSource('geojson', {
        "type": "geojson",
        "data": geojson
    });

// Add styles to the map
    map.addLayer({
        id: 'measure-points',
        type: 'circle',
        source: 'geojson',
        paint: {
            'circle-radius': 1.5,
            'circle-color': '#00a800'
        },
        filter: ['in', '$type', 'Point']
    });
    map.addLayer({
        id: 'measure-lines',
        type: 'line',
        source: 'geojson',
        layout: {
            'line-cap': 'round',
            'line-join': 'round'
        },
        paint: {
            'line-color': '#00a800',
            'line-width': 3.0
        },
        filter: ['in', '$type', 'LineString']
    });
});

function drawTracks(loc) {
    if (geojson.features.length > 1) geojson.features.pop();
    let point = {
        "type": "Feature",
        "geometry": {
            "type": "Point",
            "coordinates": loc.getLngLat()
        },
        "properties": {
            "id": String(new Date().getTime())
        }
    };

    geojson.features.push(point);

    if (geojson.features.length > 1) {
        linestring.geometry.coordinates = geojson.features.map(function (point) {
            return point.geometry.coordinates;
        });
        geojson.features.push(linestring);
    }

    map.getSource('geojson').setData(geojson);
}


function geo_success(position) {
    console.log(position.timestamp);
    console.log(position.coords.latitude, position.coords.longitude, position.coords.accuracy);
    console.log(position.coords.altitude, position.coords.altitudeAccuracy);
    console.log(position.coords.heading, position.coords.speed);

    current = new LngLat(position.coords.longitude, position.coords.latitude);
    map.setCenter(current.getLngLat());
    if (run === null) {
        markerStart.setLngLat(current.getLngLat()).addTo(map);
        if (position.coords.accuracy < 20) {
            document.getElementById("generateDestination").disabled = false;
            startLocation = new LngLat(current.getLongitude(), current.getLatitude());
        } else {
            displayMessage("Please wait for accurate positioning");
        }
    } else {
        if (position.coords.accuracy < 20) {
            let toStartLocation = current.distanceTo(startLocation);
            let toDestination = current.distanceTo(destination);
            popupStart.setHTML(`<span>${Math.round(toStartLocation)} m</span>`);
            popupDestination.setHTML(`<span>${Math.round(toDestination)} m</span>`);
            if (restart) {
                markerCurrent.setLngLat(current.getLngLat()).addTo(map);
                if (toStartLocation < 10) {
                    restart = false;
                    markerCurrent.remove();
                    document.getElementById("beginRun").disabled = false;
                }
            } else {
                run.addTrack(current);
                drawTracks(current);
                if (run.isArrived()) {
                    clearInterval(runid);
                    navigator.geolocation.clearWatch(wpid);
                    document.getElementById("saveRun").disabled = false;
                }
            }
        }
    }
}

function geo_error() {
    displayMessage("Sorry, no position available.");
}

function randomDestination(currentLocation) {
    return new LngLat(currentLocation.getLongitude() + (Math.random() * 2 - 1), currentLocation.getLatitude() + Math.random() * 2 - 1);
}

function generateDestination() {
    let dis = Infinity;
    do {
        destination = randomDestination(startLocation);
        dis = startLocation.distanceTo(destination);
    } while (dis < 60 || dis > 150);
    markerEnd.setLngLat(destination.getLngLat()).addTo(map);
    document.getElementById("beginRun").disabled = false
}

let runid = undefined;

function beginRun() {
    run = new Run(startLocation, destination);
    let toStartLocation = current.distanceTo(startLocation);
    let toDestination = current.distanceTo(destination);
    popupStart.setLngLat(startLocation.getLngLat()).addTo(map).setHTML(`<span>${Math.round(toStartLocation)} m</span>`);
    popupDestination.setLngLat(destination.getLngLat()).addTo(map).setHTML(`<span>${Math.round(toDestination)} m</span>`);
    run.addTrack(current);
    drawTracks(current);
    document.getElementById("generateDestination").disabled = true;
    document.getElementById("beginRun").disabled = true;
    runid = setInterval(function () {
        let dis = run.getDistance();
        let time = (new Date() - run.startClock) / 1000;
        document.getElementById("showDistance").innerHTML = `${Math.round(dis)} m`;
        document.getElementById("showTime").innerHTML = formatTime(time);
    });
}

function saveRun() {
    run.setNickname(document.getElementById("nickname").value);
    if (runIndex in savedRuns) {
        savedRuns[runIndex] = run;
    } else {
        savedRuns.push(run);
    }
    localStorage.setItem(APP_PREFIX + "-savedRuns", JSON.stringify(savedRuns));
    window.location.href = "index.html";
}